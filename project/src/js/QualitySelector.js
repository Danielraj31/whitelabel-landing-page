/**
 * All the possible quality settings that must be accounted for
 */
var QualitySettings = {
    AUTO : "auto",
    LOW : "360p",
    MEDIUM : "540p",
    HIGH : "720p",
    FULL_HD : "1080p"
}

var QualitySelector = {
    /**
     * Initialise the quality selector
     */
    initialise: function(startingQuality) {
        if (VideoPlayerInterface.iframeWindow.rtc != null && VideoPlayerInterface.iframeWindow.rtc.player.quality.getLastResolution() != null) {
            QualitySelector.loaded = true;

            // Events for quality selector
            QualitySelector.events.initialise();

            QualitySelector.showEnabledResolutions();

            // Set initial state.
            QualitySelector.removeItemSelection();
            if (VideoPlayerInterface.iframeWindow.rtc.player.quality.getAuto()) {
                QualitySelector.setAutoTrue();
            } else {
                QualitySelector.setSelected(VideoPlayerInterface.iframeWindow.rtc.player.quality.getSelected());
            }

            // Update auto selected resolution during playback.
            VideoPlayerInterface.iframeWindow.rtc.events.subscribe('player.quality.lastResolution', function (e, data) {
                if (VideoPlayerInterface.iframeWindow.rtc.player.quality.getAuto()) {
                    QualitySelector.removeItemSelection();
                    $("#jsautoTickIcon").show();
                    $("#js" + data.lastResolution + "AutoIcon").show();
                    QualitySelector.setButtonHd(data.lastResolution);
                }
            });
        }
    },

    loaded: false,

    setButtonHd: function(quality) {
        var hdOn = VideoPlayerInterface.iframeWindow.rtc.player.quality.isHd(quality);
        $btn = $('#jsSettingsButtonIcon, #jsQualitySelectorButtonIcon');
        if (hdOn) {
            $btn.addClass('hd');
            $btn.removeClass('sd');
        } else {
            $btn.addClass('sd');
            $btn.removeClass('hd');
        }
    },

    setAutoTrue: function() {
        $("#jsautoTickIcon").show();

        var last = VideoPlayerInterface.iframeWindow.rtc.player.quality.getLastResolution();
        if (typeof last === "string") {
            $("#js" + last + "AutoIcon").show();
            $('.js-quality-text').text(' (' + last + ')');
            QualitySelector.setButtonHd(last);
        }
    },

    setSelected: function(quality) {
        $("#js" + quality + "TickIcon").show();
        $('.js-quality-text').text(' (' + quality + ')');
        QualitySelector.setButtonHd(quality);
    },

    removeItemSelection: function() {
        for (var quality in QualitySettings) {
            $("#js" + QualitySettings[quality] + "AutoIcon").hide();
            $("#js" + QualitySettings[quality] + "TickIcon").hide();
        }
    },

    showEnabledResolutions: function() {
        // Show only enabled resolutions.
        var show = VideoPlayerInterface.iframeWindow.rtc.player.quality.getAvailable();
        $(".timeline-control-quality__item, .timeline-control-quality-only__item").each(function(i, el) {
            var $el = $(el);
            var res = $el.data('quality');
            if ($.inArray(res, show) === -1 && res !== 'auto') {
                $el.hide();
            }
        });
    },

    /**
     * Define the events for the quality selector
     */
    events: {
        /**
         * Link up the events and the event handlers
         */
        initialise: function() {
            //Events for Full Menu
            $(".timeline-control-quality__item").click(QualitySelector.events.selectQualityFull);

            /**
             * Hover states for quality only menu and Back button is set in SettingsPanel -> initMenu()
             * to prevent conflicts
             */

            //Events for Both Menus
            $("#jsQualitySelectorButtonPopout").click(QualitySelector.events.selectQuality);
        },

        selectQualityFull: function(e) {
            var qualityButton = $(this);
            var quality = qualityButton.data("quality");

            QualitySelector.removeItemSelection();
            if (quality == "auto") {
                VideoPlayerInterface.iframeWindow.rtc.player.quality.setAutoTrue();
                QualitySelector.setAutoTrue();
            } else {
                VideoPlayerInterface.iframeWindow.rtc.player.quality.setSelected(quality);
                QualitySelector.setSelected(quality);
            }
        },

        closeQualityMenu: function (e) {
            $('#jsSettingsButtonPopout').show();
            $('#jsQualitySelectorPopout').hide();
        },

        selectQuality: function(e) {
            var qualityButton = $(e.target);
            var quality = qualityButton.data("quality");

            QualitySelector.removeItemSelection();
            if (quality == "auto") {
                VideoPlayerInterface.iframeWindow.rtc.player.quality.setAutoTrue();
                QualitySelector.setAutoTrue();
            } else {
                VideoPlayerInterface.iframeWindow.rtc.player.quality.setSelected(quality);
                QualitySelector.setSelected(quality);
            }
        },

        /**
         * Display the quality selector control when the user hovers over the quality icon or quality menu
         */
        qualityButtonHoverInEventHandler: function(e) {
            $('#jsQualitySelectorPopout').show();
        },

        /**
         * Hide the quality selector control when the user moves their mouse away from the quality icon or quality menu
         */
        qualityButtonHoverOutEventHandler: function(e) {
            $('#jsQualitySelectorPopout').hide();
        }
    }
};
